/************************************************************************\
* 文件：SFile.cpp
*
* 说明：通用文件类实现
*
* 作者：Seven
\************************************************************************/ 

#include "../include/SFile.h"
#include <io.h>

S_USE_SL()

S_BEGIN_PIMPL(SFile)
	HANDLE	m_hFile;
	SFile::FileNames m_Names;
S_END_PIMPL(SFile)

SFile::SFile() : S_INIT_PIMPL()
{
	m_PIMPL->m_Names.szFullName[0] = _T('\0');
	m_PIMPL->m_Names.pExtName = m_PIMPL->m_Names.pFileName = m_PIMPL->m_Names.szFullName;
	m_PIMPL->m_hFile = NULL;
}

SFile::~SFile()
{
	Close();

	S_CLEAN_PIMPL();
}

BOOL SFile::Open(LPCTSTR pFileName, DWORD access_mode, DWORD share_mode /* = shareNone */,
				 DWORD file_flag /* = flagNormal */, BOOL bInherit /* = FALSE */)
{
	S_ASSERT(pFileName != NULL);
	// FFLAG_RANDOM_ACCESS 与 FFLAG_SEQUENTIAL_SCAN 互斥
	S_ASSERT((file_flag & (flagRandomAccess|flagSequentialScan)) != (flagRandomAccess|flagSequentialScan));

	// 关闭以前的文件
	if(m_PIMPL->m_hFile)
		Close();

	// Get Name
	if(!GetNames(pFileName, m_PIMPL->m_Names))
	{
		m_PIMPL->m_Names.szFullName[0] = _T('\0');
		m_PIMPL->m_Names.pExtName = m_PIMPL->m_Names.pFileName = m_PIMPL->m_Names.szFullName;
		return FALSE;
	}

	SECURITY_ATTRIBUTES sa;
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = bInherit;

	m_PIMPL->m_hFile = ::CreateFile(pFileName, access_mode, share_mode,
		&sa, OPEN_EXISTING, file_flag | FILE_ATTRIBUTE_NORMAL, NULL);
	if(m_PIMPL->m_hFile == INVALID_HANDLE_VALUE)
	{
		S_ASSERT(m_PIMPL->m_hFile != INVALID_HANDLE_VALUE);
		
		m_PIMPL->m_Names.szFullName[0] = _T('\0');
		m_PIMPL->m_Names.pExtName = m_PIMPL->m_Names.pFileName = m_PIMPL->m_Names.szFullName;
		m_PIMPL->m_hFile = NULL;
		return FALSE;
	}
	return TRUE;
}

BOOL SFile::Create(LPCTSTR pFileName, DWORD access_mode, DWORD share_mode /* = shareNone */,
				   DWORD file_flag /* = flagNormal */, BOOL bInherit /* = FALSE */,
				   const SECURITY_DESCRIPTOR* pSecurityDescriptor /* = NULL */)
{
	S_ASSERT(pFileName != NULL);
	// FFLAG_RANDOM_ACCESS 与 FFLAG_SEQUENTIAL_SCAN 互斥
	S_ASSERT((file_flag & (flagRandomAccess|flagSequentialScan)) != (flagRandomAccess|flagSequentialScan));

	// 关闭以前的文件
	if(m_PIMPL->m_hFile)
		Close();

	// Get Name
	if(!GetNames(pFileName, m_PIMPL->m_Names))
	{
		m_PIMPL->m_Names.szFullName[0] = _T('\0');
		m_PIMPL->m_Names.pExtName = m_PIMPL->m_Names.pFileName = m_PIMPL->m_Names.szFullName;
		return FALSE;
	}

	SECURITY_ATTRIBUTES sa;
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = static_cast<LPVOID>(const_cast<SECURITY_DESCRIPTOR*>(pSecurityDescriptor));
	sa.bInheritHandle = bInherit;

	m_PIMPL->m_hFile = ::CreateFile(pFileName, access_mode, share_mode,
		&sa, CREATE_NEW, file_flag | FILE_ATTRIBUTE_NORMAL, NULL);
	if(m_PIMPL->m_hFile == INVALID_HANDLE_VALUE)
	{
		S_ASSERT(m_PIMPL->m_hFile != INVALID_HANDLE_VALUE);
	
		m_PIMPL->m_Names.szFullName[0] = _T('\0');
		m_PIMPL->m_Names.pExtName = m_PIMPL->m_Names.pFileName = m_PIMPL->m_Names.szFullName;
		m_PIMPL->m_hFile = NULL;
		return FALSE;
	}

	return TRUE;
}

void SFile::Close()
{
	if(m_PIMPL->m_hFile)
	{
		S_VERIFY(::CloseHandle(m_PIMPL->m_hFile));
		m_PIMPL->m_Names.szFullName[0] = _T('\0');
		m_PIMPL->m_Names.pExtName = m_PIMPL->m_Names.pFileName = m_PIMPL->m_Names.szFullName;
		m_PIMPL->m_hFile = NULL;
	}
}

BOOL SFile::IsValid()const
{
	return m_PIMPL->m_hFile ? TRUE : FALSE;
}

HANDLE SFile::Detach()
{
	HANDLE h = m_PIMPL->m_hFile;
	m_PIMPL->m_Names.szFullName[0] = _T('\0');
	m_PIMPL->m_Names.pExtName = m_PIMPL->m_Names.pFileName = m_PIMPL->m_Names.szFullName;
	m_PIMPL->m_hFile = NULL;
	return h;
}

HANDLE SFile::GetHandle()const
{
	return m_PIMPL->m_hFile;
}

BOOL SFile::GetAttributes(LPCTSTR pFileName, DWORD& file_attr)
{
	S_ASSERT(pFileName != NULL);

	// Full Name
	TCHAR szFullName[MAX_NAMEBUF];
	DWORD namelen = ::GetFullPathName(pFileName, MAX_NAMEBUF, szFullName, NULL);
	if(!namelen || namelen >= MAX_NAMEBUF)
	{
		S_ASSERT(namelen > 0 && namelen < MAX_NAMEBUF);
		return FALSE;
	}

	file_attr = ::GetFileAttributes(szFullName);
	if(file_attr == INVALID_FILE_ATTRIBUTES)
	{
		S_ASSERT(file_attr != INVALID_FILE_ATTRIBUTES);
		return FALSE;
	}

	return TRUE;
}

BOOL SFile::SetAttributes(LPCTSTR pFileName, DWORD file_attr)
{
	S_ASSERT(pFileName != NULL);

	// Full Name
	TCHAR szFullName[MAX_NAMEBUF];
	DWORD namelen = ::GetFullPathName(pFileName, MAX_NAMEBUF, szFullName, NULL);
	if(!namelen || namelen >= MAX_NAMEBUF)
	{
		S_ASSERT(namelen > 0 && namelen < MAX_NAMEBUF);
		return FALSE;
	}

	if(!::SetFileAttributes(szFullName, file_attr))
		return FALSE;

	return TRUE;
}

BOOL SFile::Clean(LPCTSTR pFileName)
{
	S_ASSERT(pFileName != NULL);

	// Full Name
	TCHAR szFullName[MAX_NAMEBUF];
	DWORD namelen = ::GetFullPathName(pFileName, MAX_NAMEBUF, szFullName, NULL);
	if(!namelen || namelen >= MAX_NAMEBUF)
	{
		S_ASSERT(namelen > 0 && namelen < MAX_NAMEBUF);
		return FALSE;
	}

	HANDLE hFile = ::CreateFile(szFullName, GENERIC_WRITE, 0, NULL,
		TRUNCATE_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if(hFile == INVALID_HANDLE_VALUE)
	{
		S_ASSERT(hFile != INVALID_HANDLE_VALUE);
		return FALSE;
	}

	::CloseHandle(hFile);
	return TRUE;
}

BOOL SFile::GetTime(STime* pCreateTime, STime* pModifyTime, STime* pAccessTime)const
{
	S_ASSERT(m_PIMPL->m_hFile != NULL);

	FILETIME createTime, modifyTime, accessTime;
	if(::GetFileTime(m_PIMPL->m_hFile,
					pCreateTime ? &createTime : NULL,
					pModifyTime ? &modifyTime : NULL,
					pAccessTime ? &accessTime : NULL))
	{
		if(pCreateTime && !pCreateTime->SetTime(createTime))
			return FALSE;
		if(pModifyTime && !pModifyTime->SetTime(modifyTime))
			return FALSE;
		if(pAccessTime && !pAccessTime->SetTime(accessTime))
			return FALSE;
		return TRUE;
	}
	
	return FALSE;
}

BOOL SFile::SetTime(const STime* pCreateTime, const STime* pModifyTime, const STime* pAccessTime)
{
	S_ASSERT(m_PIMPL->m_hFile != NULL);

	FILETIME createTime, modifyTime, accessTime;
	FILETIME* pCT = NULL, *pMT = NULL, *pAT = NULL;
	if(pCreateTime)
	{
		if(!pCreateTime->GetTime(createTime))
			return FALSE;
		else
			pCT = &createTime;
	}
	if(pModifyTime)
	{
		if(!pModifyTime->GetTime(modifyTime))
			return FALSE;
		else
			pMT = &modifyTime;
	}
	if(pAccessTime)
	{
		if(!pAccessTime->GetTime(accessTime))
			return FALSE;
		else
			pAT = &accessTime;
	}

	if(!::SetFileTime(m_PIMPL->m_hFile, pCT, pAT, pMT))
		return FALSE;

	return TRUE;
}

BOOL SFile::Write(const void* pData, DWORD datalenBytes, DWORD* pWriteLen /* = NULL */)
{
	S_ASSERT(pData != NULL);
	S_ASSERT(m_PIMPL->m_hFile != NULL);
	DWORD dwWrite;
	return ::WriteFile(m_PIMPL->m_hFile, pData, datalenBytes, pWriteLen ? pWriteLen : &dwWrite, NULL);
}

BOOL SFile::Read(void* pData, DWORD datalenBytes, DWORD* pReadLen)
{
	S_ASSERT(pData != NULL);
	S_ASSERT(m_PIMPL->m_hFile != NULL);
	DWORD dwRead;
	return ::ReadFile(m_PIMPL->m_hFile, pData, datalenBytes, pReadLen ? pReadLen : &dwRead, NULL);
}

BOOL SFile::Flush()
{
	S_ASSERT(m_PIMPL->m_hFile != NULL);
	return ::FlushFileBuffers(m_PIMPL->m_hFile);
}

BOOL SFile::SetSize(UINT64 size)
{
	// 保存原始位置指针
	UINT64 lastpos;
	if(!Seek(seekCurrent, 0, &lastpos))
		return FALSE;

	// 移动位置指针到新位置
	if(!Seek(seekBegin, size))
		return FALSE;

	// 保存文件结尾符
	BOOL bRet;
	S_VERIFY(bRet = ::SetEndOfFile(m_PIMPL->m_hFile));

	// 还原位置指针
	Seek(seekBegin, lastpos);

	return bRet;
}

BOOL SFile::Seek(SeekPosition from, INT64 offset /* = 0 */, UINT64* pNewPosition /* = NULL */)
{
	S_ASSERT(m_PIMPL->m_hFile != NULL);

	LARGE_INTEGER liOff;
	liOff.QuadPart = offset;
	liOff.LowPart = ::SetFilePointer(m_PIMPL->m_hFile, liOff.LowPart, &liOff.HighPart, from);
	
	// 因为INVALID_SET_FILE_POINTER是合法的文件内指针位置，所以需额外GetLastError()来确定是否失败
	if(liOff.LowPart  == INVALID_SET_FILE_POINTER && ::GetLastError())
	{
		S_ASSERT(liOff.LowPart != INVALID_SET_FILE_POINTER || ::GetLastError() == NO_ERROR);
		return FALSE;
	}		

	if(pNewPosition)
		*pNewPosition = liOff.QuadPart;

	return TRUE;
}

BOOL SFile::GetSize(UINT64& size)const
{
	S_ASSERT(m_PIMPL->m_hFile != NULL);
	
	return ::GetFileSizeEx(m_PIMPL->m_hFile, reinterpret_cast<PLARGE_INTEGER>(&size));
}

LPCTSTR SFile::GetFullName()const
{
	S_ASSERT(m_PIMPL->m_hFile != NULL);
	return m_PIMPL->m_Names.szFullName;
}

LPCTSTR SFile::GetName()const
{
	S_ASSERT(m_PIMPL->m_hFile != NULL);
	return m_PIMPL->m_Names.pFileName;
}

LPCTSTR SFile::GetExtName()const
{
	S_ASSERT(m_PIMPL->m_hFile != NULL);
	return m_PIMPL->m_Names.pExtName;
}

BOOL SFile::Copy(LPCTSTR pSrcFile, LPCTSTR pNewFile, BOOL bFailExist /* = TRUE */)
{
	S_ASSERT(pSrcFile != NULL);
	S_ASSERT(pNewFile != NULL);

	// Full Name
	TCHAR szFullSrcName[MAX_NAMEBUF];
	TCHAR szFullNewName[MAX_NAMEBUF];
	DWORD namelen;
	namelen= ::GetFullPathName(pSrcFile, MAX_NAMEBUF, szFullSrcName, NULL);
	if(!namelen || namelen >= MAX_NAMEBUF)
	{
		S_ASSERT(namelen > 0 && namelen < MAX_NAMEBUF);
		return FALSE;
	}
	namelen= ::GetFullPathName(pNewFile, MAX_NAMEBUF, szFullNewName, NULL);
	if(!namelen || namelen >= MAX_NAMEBUF)
	{
		S_ASSERT(namelen > 0 && namelen < MAX_NAMEBUF);
		return FALSE;
	}

	return ::CopyFile(szFullSrcName, szFullNewName, bFailExist);
}

BOOL SFile::IsExist(LPCTSTR pFileName)
{
	S_ASSERT(pFileName != NULL);
	
	// Full Name
	TCHAR szFullName[MAX_NAMEBUF];
	DWORD namelen = ::GetFullPathName(pFileName, MAX_NAMEBUF, szFullName, NULL);
	if(!namelen || namelen >= MAX_NAMEBUF)
	{
		S_ASSERT(namelen > 0 && namelen < MAX_NAMEBUF);
		return FALSE;
	}

	return _taccess(szFullName, 0) != -1 ? TRUE : FALSE;
}

BOOL SFile::GetNames(LPCTSTR pFileName, S_OUT FileNames& names)
{
	S_ASSERT(pFileName != NULL);

	DWORD namelen = ::GetFullPathName(pFileName, MAX_NAMEBUF, names.szFullName, const_cast<LPTSTR*>(&names.pFileName));
	if(!namelen || namelen >= MAX_NAMEBUF)
	{
		S_ASSERT(namelen > 0 && namelen < MAX_NAMEBUF);
		return FALSE;
	}

	// 得到扩展名
	if(names.pFileName)
	{
		namelen = _tcslen(names.pFileName);
		names.pExtName = names.pFileName + namelen - 1;
		while(names.pExtName != names.pFileName)
		{
			if(*names.pExtName == _T('.'))
			{
				++ names.pExtName;
				return TRUE;
			}
			-- names.pExtName;
		}

		names.pExtName = names.pFileName + namelen;
	}
	else
	{
		names.pFileName = names.szFullName + _tcslen(names.szFullName);
		names.pExtName = names.pFileName;
	}
	return TRUE;
}

BOOL SFile::Delete(LPCTSTR pFileName)
{
	S_ASSERT(pFileName != NULL);

	// Full Name
	TCHAR szFullName[MAX_NAMEBUF];
	DWORD namelen = ::GetFullPathName(pFileName, MAX_NAMEBUF, szFullName, NULL);
	if(!namelen || namelen >= MAX_NAMEBUF)
	{
		S_ASSERT(namelen > 0 && namelen < MAX_NAMEBUF);
		return FALSE;
	}

	return ::DeleteFile(szFullName);
}

BOOL SFile::Move(LPCTSTR pOldFile, LPCTSTR pNewFile)
{
	S_ASSERT(pOldFile != NULL);
	S_ASSERT(pNewFile != NULL);

	// Full Name
	TCHAR szFullOldName[MAX_NAMEBUF];
	TCHAR szFullNewName[MAX_NAMEBUF];
	DWORD namelen;
	namelen = ::GetFullPathName(pOldFile, MAX_NAMEBUF, szFullOldName, NULL);
	if(!namelen || namelen >= MAX_NAMEBUF)
	{
		S_ASSERT(namelen > 0 && namelen < MAX_NAMEBUF);
		return FALSE;
	}
	namelen = ::GetFullPathName(pNewFile, MAX_NAMEBUF, szFullNewName, NULL);
	if(!namelen || namelen >= MAX_NAMEBUF)
	{
		S_ASSERT(namelen > 0 && namelen < MAX_NAMEBUF);
		return FALSE;
	}


	return ::MoveFile(szFullOldName, szFullNewName);
}