/************************************************************************\
* 文件：SObjectAutoPtr.cpp
*
* 说明：自动指针类实现
*
* 作者：Seven
\************************************************************************/ 

#include "../include/SObject.h"
#include "../include/SObjectAutoPtr.h"
S_USE_SL()

// PIMPL列表
S_BEGIN_PIMPL(SObjectAutoPtr)
SObject* m_pObject;
S_END_PIMPL(SObjectAutoPtr)

SObjectAutoPtr::SObjectAutoPtr(SObject* ptr /* = NULL */) : S_INIT_PIMPL()
{
	m_PIMPL->m_pObject = ptr;
}

SObjectAutoPtr::SObjectAutoPtr(const SObjectAutoPtr& ptr) : S_INIT_PIMPL()
{
	// 接管目标指针
	m_PIMPL->m_pObject = ptr.m_PIMPL->m_pObject;

	// 目标脱离
	ptr.m_PIMPL->m_pObject = NULL;
}

SObjectAutoPtr::~SObjectAutoPtr()
{
	Delete();

	S_CLEAN_PIMPL();
}

void SObjectAutoPtr::Delete()
{
	if(m_PIMPL->m_pObject)
	{
		m_PIMPL->m_pObject->DeleteThis();
		m_PIMPL->m_pObject = NULL;
	}
}

SObject* SObjectAutoPtr::Detach()
{
	SObject* pObj = m_PIMPL->m_pObject;
	m_PIMPL->m_pObject = NULL;
	return pObj;
}

BOOL SObjectAutoPtr::IsValid()const
{
	return m_PIMPL->m_pObject ? TRUE : FALSE;
}

SObject* SObjectAutoPtr::GetObject()const
{
	return m_PIMPL->m_pObject;
}

SObjectAutoPtr::operator SObject*()const
{
	return m_PIMPL->m_pObject;
}

SObjectAutoPtr& SObjectAutoPtr::operator=(const SObjectAutoPtr& ptr)
{
	// 清理之前管理的指针
	if(m_PIMPL->m_pObject)
		m_PIMPL->m_pObject->DeleteThis();
	
	// 接管目标指针
	m_PIMPL->m_pObject = ptr.m_PIMPL->m_pObject;

	// 目标脱离
	ptr.m_PIMPL->m_pObject = NULL;

	return *this;
}