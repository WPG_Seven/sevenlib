/************************************************************************\
* 文件：SType.cpp
*
* 说明：动态类型类实现
*
* 作者：Seven
\************************************************************************/ 

#include "../include/SObject.h"
S_USE_SL()

// PIMPL类
class SType::S_PIMPL(SType)
{
public:
	S_PIMPL(SType)(){}

	LPCTSTR m_lpszName;				// 类名
	UINT m_uSize;					// 大小
	CreateObjectFunc m_pfnCreate;	// 动态创建
};

SType::S_PIMPL(SType)* SType::CreatePIMPL(const SType* pPannel)
{ return new S_PIMPL(SType)(); }

SType::SType(LPCTSTR lpszName, UINT uSize, CreateObjectFunc fnCreate) : S_INIT_PIMPL()
{	
	m_PIMPL->m_lpszName = lpszName;
	m_PIMPL->m_uSize = uSize;
	m_PIMPL->m_pfnCreate = fnCreate;
}

SType::~SType()
{
	S_CLEAN_PIMPL();
}

SObject* SType::CreateObject()
{
	return (*(m_PIMPL->m_pfnCreate))();
}

LPCTSTR SType::GetName()const
{
	return m_PIMPL->m_lpszName;
}

UINT SType::GetSize()const
{
	return m_PIMPL->m_uSize;
}
