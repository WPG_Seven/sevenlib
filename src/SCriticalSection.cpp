/************************************************************************\
* 文件：SCriticalSection.cpp
*
* 说明：多线程同步-关键区类实现
*
* 作者：Seven
\************************************************************************/

#include "../include/SCriticalSection.h"
S_USE_SL()

S_BEGIN_PIMPL(SCriticalSection)
CRITICAL_SECTION m_cs;
S_END_PIMPL(SCriticalSection)

SCriticalSection::SCriticalSection() : S_INIT_PIMPL()
{
	::InitializeCriticalSection(&(m_PIMPL->m_cs));
}

SCriticalSection::~SCriticalSection()
{
	::DeleteCriticalSection(&(m_PIMPL->m_cs));

	S_CLEAN_PIMPL();
}

void SCriticalSection::Enter()
{
	::EnterCriticalSection(&(m_PIMPL->m_cs));
}

void SCriticalSection::Leave()
{
	::LeaveCriticalSection(&(m_PIMPL->m_cs));
}

CRITICAL_SECTION* SCriticalSection::GetCriticalSection()const
{
	return &(m_PIMPL->m_cs);
}