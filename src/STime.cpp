/************************************************************************\
* 文件：SStream.cpp
*
* 说明：时间类实现
*
* 作者：Seven
\************************************************************************/ 

#include "../include/STime.h"
S_USE_SL()

S_BEGIN_PIMPL(STime)
INT64 m_time;
S_END_PIMPL(STime)

STime::STime(INT64 time /* = 0 */) : S_INIT_PIMPL()
{
	S_ASSERT(time >= 0);
	m_PIMPL->m_time = time;
}

STime::STime(const STime& point) : S_INIT_PIMPL()
{
	S_ASSERT(point.GetTime() >= 0);
	m_PIMPL->m_time = point.m_PIMPL->m_time;
}

STime::~STime()
{
	S_CLEAN_PIMPL();
}

BOOL STime::SetTime(INT64 time)
{
	S_ASSERT(time >= 0);
	m_PIMPL->m_time = time;

	return time >= 0 ? TRUE : FALSE;
}

BOOL STime::SetTime(const tm& ttm)
{
	S_ASSERT(ttm.tm_year >= 70);
	S_ASSERT(ttm.tm_mon >= 0 && ttm.tm_mon <= 11);
	S_ASSERT(ttm.tm_mday >= 1 && ttm.tm_mday <= 31);
	S_ASSERT(ttm.tm_hour >= 0 && ttm.tm_hour <= 23);
	S_ASSERT(ttm.tm_min >= 0 && ttm.tm_min <= 59);
	S_ASSERT(ttm.tm_sec >= 0 && ttm.tm_sec <= 59);

	m_PIMPL->m_time = ::_mktime64((tm*)&ttm);
	S_ASSERT(m_PIMPL->m_time >= 0);

	return m_PIMPL->m_time >= 0 ? TRUE : FALSE;
}

BOOL STime::SetTime(int nYear, int nMonth, int nDay, int nHour, int nMin, int nSec, int nDST /* = -1 */)
{
	tm atm;
	atm.tm_sec = nSec;
	atm.tm_min = nMin;
	atm.tm_hour = nHour;
	atm.tm_mday = nDay;
	atm.tm_mon = nMonth - 1;        // tm_mon is 0 based
	atm.tm_year = nYear - 1900;     // tm_year is 1900 based
	atm.tm_isdst = nDST;

	return SetTime(atm);
}

BOOL STime::SetTime(const SYSTEMTIME& st, int nDST /* = -1 */)
{
	tm atm;
	atm.tm_sec = st.wSecond;
	atm.tm_min = st.wMinute;
	atm.tm_hour = st.wHour;
	atm.tm_mday = st.wDay;
	atm.tm_mon = st.wMonth - 1;        // tm_mon is 0 based
	atm.tm_year = st.wYear - 1900;     // tm_year is 1900 based
	atm.tm_isdst = nDST;

	return SetTime(atm);
}

BOOL STime::SetTime(const FILETIME& ft, int nDST /* = -1 */)
{
	// first convert file time (UTC time) to local time
	FILETIME localTime;
	if (!FileTimeToLocalFileTime(&ft, &localTime))
	{
		m_PIMPL->m_time = -1;
		return FALSE;
	}

	// then convert that time to system time
	SYSTEMTIME st;
	if (!FileTimeToSystemTime(&localTime, &st))
	{
		m_PIMPL->m_time = -1;
		return FALSE;
	}

	return SetTime(st);
}

BOOL STime::CurrentTime()
{
	m_PIMPL->m_time = ::_time64(NULL);
	S_ASSERT(m_PIMPL->m_time >= 0);

	return m_PIMPL->m_time >= 0 ? TRUE : FALSE;
}

INT64 STime::GetTime()const
{
	S_ASSERT(m_PIMPL->m_time >= 0);
	return m_PIMPL->m_time;
}

BOOL STime::GetTime(tm& ttm)const
{
	S_ASSERT(m_PIMPL->m_time >= 0);

	if(::_localtime64_s(&ttm, &m_PIMPL->m_time))
		return FALSE;
	
	return TRUE;
}

BOOL STime::GetTime(SYSTEMTIME& st)const
{
	tm atm;
	if(!GetTime(atm))
		return FALSE;

	st.wYear = atm.tm_year + 1900;
	st.wMonth = atm.tm_mon + 1;
	st.wDay = atm.tm_mday;
	st.wHour = atm.tm_hour;
	st.wMinute = atm.tm_min;
	st.wSecond = atm.tm_sec;
	st.wDayOfWeek = atm.tm_wday;
	st.wMilliseconds = 0;

	return TRUE;
}

BOOL STime::GetTime(FILETIME& ft)const
{
	SYSTEMTIME st;
	if(!GetTime(st))
		return FALSE;

	// convert system time to local file time
	FILETIME localTime;
	if(!SystemTimeToFileTime(&st, &localTime))
		return FALSE;

	// convert local file time to UTC file time
	if(!LocalFileTimeToFileTime(&localTime, &ft))
		return FALSE;

	return TRUE;
}

int STime::GetYear()const
{
	tm atm;
	if(!GetTime(atm))
		return -1;

	return (atm.tm_year + 1900);
}

int STime::GetMonth()const
{
	tm atm;
	if(!GetTime(atm))
		return -1;

	return (atm.tm_mon + 1);
}

int STime::GetDay()const
{
	tm atm;
	if(!GetTime(atm))
		return -1;

	return atm.tm_mday;
}

int STime::GetHour()const
{
	tm atm;
	if(!GetTime(atm))
		return -1;

	return atm.tm_hour;
}

int STime::GetMinute()const
{
	tm atm;
	if(!GetTime(atm))
		return -1;

	return atm.tm_min;
}

int STime::GetSecond()const
{
	tm atm;
	if(!GetTime(atm))
		return -1;

	return atm.tm_sec;
}

int STime::GetDayOfWeek()const
{
	tm atm;
	if(!GetTime(atm))
		return -1;

	return atm.tm_wday;
}

int STime::GetDayOfYear()const
{
	tm atm;
	if(!GetTime(atm))
		return -1;

	return atm.tm_yday;
}

STime& STime::operator=(const STime& point)
{
	S_ASSERT(point.m_PIMPL->m_time >= 0);
	m_PIMPL->m_time = point.m_PIMPL->m_time;
	
	return *this;
}

STime& STime::operator+=(const STimeSpan& span)
{
	S_ASSERT(m_PIMPL->m_time >= 0);

	if(m_PIMPL->m_time >= 0)
	{
		m_PIMPL->m_time += span.GetTotalSeconds();
		S_ASSERT(m_PIMPL->m_time >= 0);
	}

	return *this;
}

STime& STime::operator-=(const STimeSpan& span)
{
	S_ASSERT(m_PIMPL->m_time >= 0);

	if(m_PIMPL->m_time >= 0)
	{
		m_PIMPL->m_time -= span.GetTotalSeconds();
		S_ASSERT(m_PIMPL->m_time >= 0);
	}

	return *this;
}

STime STime::operator+(const STimeSpan& span)const
{
	S_ASSERT(m_PIMPL->m_time >= 0);

	if(m_PIMPL->m_time >= 0)
	{
		S_ASSERT(m_PIMPL->m_time + span.GetTotalSeconds() >= 0);
		return STime(m_PIMPL->m_time + span.GetTotalSeconds());
	}

	return STime(m_PIMPL->m_time);
}

STime STime::operator-(const STimeSpan& span)const
{
	S_ASSERT(m_PIMPL->m_time >= 0);

	if(m_PIMPL->m_time >= 0)
	{
		S_ASSERT(m_PIMPL->m_time - span.GetTotalSeconds() >= 0);
		return STime(m_PIMPL->m_time - span.GetTotalSeconds());
	}

	return STime(m_PIMPL->m_time);
}

STimeSpan STime::operator-(const STime& point)const
{
	S_ASSERT(m_PIMPL->m_time >= 0 && point.m_PIMPL->m_time >= 0);

	if(m_PIMPL->m_time >= 0 && point.m_PIMPL->m_time >= 0)
		return STimeSpan(m_PIMPL->m_time - point.m_PIMPL->m_time);

	return STimeSpan();
}

bool STime::operator==(const STime& point)const
{
	S_ASSERT(m_PIMPL->m_time >= 0 && point.m_PIMPL->m_time >= 0);

	if(m_PIMPL->m_time >= 0)
	{
		if(point.m_PIMPL->m_time >= 0)
			return m_PIMPL->m_time == point.m_PIMPL->m_time;
		else
			return false;
	}
	else
	{
		if(point.m_PIMPL->m_time >= 0)
			return false;
		else
			return true;
	}
}

bool STime::operator!=(const STime& point)const
{
	S_ASSERT(m_PIMPL->m_time >= 0 && point.m_PIMPL->m_time >= 0);

	if(m_PIMPL->m_time >= 0)
	{
		if(point.m_PIMPL->m_time >= 0)
			return m_PIMPL->m_time != point.m_PIMPL->m_time;
		else
			return true;
	}
	else
	{
		if(point.m_PIMPL->m_time >= 0)
			return true;
		else
			return false;
	}
}

bool STime::operator<(const STime& point)const
{
	S_ASSERT(m_PIMPL->m_time >= 0 && point.m_PIMPL->m_time >= 0);

	if(m_PIMPL->m_time < 0 && point.m_PIMPL->m_time < 0)
		return false;

	return m_PIMPL->m_time < point.m_PIMPL->m_time;
}

bool STime::operator>(const STime& point)const
{
	S_ASSERT(m_PIMPL->m_time >= 0 && point.m_PIMPL->m_time >= 0);

	if(m_PIMPL->m_time < 0 && point.m_PIMPL->m_time < 0)
		return false;

	return m_PIMPL->m_time > point.m_PIMPL->m_time;
}

bool STime::operator<=(const STime& point)const
{
	S_ASSERT(m_PIMPL->m_time >= 0 && point.m_PIMPL->m_time >= 0);

	if(m_PIMPL->m_time < 0 && point.m_PIMPL->m_time < 0)
		return true;

	return m_PIMPL->m_time <= point.m_PIMPL->m_time;
}

bool STime::operator>=(const STime& point)const
{
	S_ASSERT(m_PIMPL->m_time >= 0 && point.m_PIMPL->m_time >= 0);

	if(m_PIMPL->m_time < 0 && point.m_PIMPL->m_time < 0)
		return true;

	return m_PIMPL->m_time >= point.m_PIMPL->m_time;
}

BOOL STime::IsValid()const
{
	return m_PIMPL->m_time >= 0;
}