/************************************************************************\
* 文件：SevenLib.cpp
*
* 说明：基本功能类的实现
*
* 作者：Seven
\************************************************************************/ 

#include "../include/SevenLib.h"
#include "../include_p/SevenLib_p.h"

using namespace SevenLib;

//////////////////////////////////////////////////////////////////////////
// SType 实现

// 私有数据定义
S_BEGIN_PRIVATE(SType)
	LPCTSTR m_lpszName;				// 类名
	UINT m_uSize;					// 大小
	CreateObjectFunc m_pfnCreator;	// 创建器指针
	ReleaseObjectFunc m_pfnRelease;	// 销毁器指针
	SType* m_pBase;					// 基类动态类型指针
S_END_PRIVATE()

SType::SType(LPCTSTR lpszName, UINT uSize, CreateObjectFunc fnCreator, ReleaseObjectFunc fnRelease, SType* pBase) :
	S_INIT_PRIVATE(SType)
{
	S_D_PRIVATE(SType, pPrivate);
	
	pPrivate->m_lpszName = lpszName;
	pPrivate->m_uSize = uSize;
	pPrivate->m_pfnCreator = fnCreator;
	pPrivate->m_pfnRelease = fnRelease;
	pPrivate->m_pBase = pBase;
}

SObject* SType::CreateObject()
{
	return (*(GetPrivate()->m_pfnCreator))();
}

void SType::ReleaseObject(SObject* pObj)
{
	(*(GetPrivate()->m_pfnRelease))(pObj);
}

BOOL SType::IsDerivedFrom(SType* pBase)const
{
	const SType* pTypeThis = this;
	do
	{
		if(pBase == pTypeThis)
			return TRUE;
	}while(pTypeThis = pTypeThis->GetPrivate()->m_pBase);

	return FALSE;
}

LPCTSTR SType::GetName()const
{
	return GetPrivate()->m_lpszName;
}

UINT SType::GetSize()const
{
	return GetPrivate()->m_uSize;
}

SType* SType::GetBaseType()const
{
	return GetPrivate()->m_pBase;
}


//////////////////////////////////////////////////////////////////////////
// Object 实现

// 类型标识
const SType SObject::typeSObject(_T("SObject"), sizeof(SObject), SObject::CreateObject, SObject::ReleaseObject, NULL);

SObject* SObject::CreateObject()
{
	return new SObject();
}

void SObject::ReleaseObject(SObject* pObj)
{
	if(pObj) {delete pObj;} 
}

SType* SObject::GetType()const
{
	return typeof(SObject);
}

BOOL SObject::IsValid()const
{
	return TRUE;
}


//////////////////////////////////////////////////////////////////////////
// Debug 实现
#ifdef _DEBUG

#ifdef UNICODE
#define	CrtDbgReport _CrtDbgReportW
#else	// UNICODE
#define	CrtDbgReport _CrtDbgReport
#endif	// !UNICODE

void SDebug::Log(LPCTSTR pFormat, ...)
{
	if(!pFormat)
		return;

	// 不定参数
	TCHAR szBuf[S_MAXBUF] = {0};
	va_list args;
	va_start(args, pFormat);
#pragma warning(push)
#pragma warning(disable: 4996)
	_vsntprintf(szBuf, S_MAXBUF, pFormat, args);
	_tcscpy(szBuf + S_MAXBUF - 4, _T("..."));	// 超过缓冲长度时自动截断处理
#pragma warning(pop)
	va_end(args);

	// 输出到调试窗口
	::OutputDebugString(szBuf);
}

BOOL SDebug::Report(LPCTSTR pFileName, int nLine, ReportType type, LPCTSTR pFormat /* = NULL */, ...)
{
	TCHAR* pBuf = NULL;

	// 需要格式化输出时
	if(pFormat)
	{
		// 不定参数
		pBuf = new TCHAR[S_MAXBUF];
		memset(pBuf, 0, S_MAXBUF);
		va_list args;
		va_start(args, pFormat);
#pragma warning(push)
#pragma warning(disable: 4996)
		_vsntprintf(pBuf, S_MAXBUF, pFormat, args);
		_tcscpy(pBuf + S_MAXBUF - 4, _T("..."));		// 超过缓冲长度时自动截断处理
#pragma warning(pop)
		va_end(args);
	}

	// 防止消息队列中有 WM_QUIT 时不显示对话框
	MSG msg;
	BOOL bQuit = ::PeekMessage(&msg, NULL, WM_QUIT, WM_QUIT, PM_REMOVE);
	BOOL bResult = CrtDbgReport(type, pFileName, nLine, NULL, pBuf);
	if(pBuf)
		delete[] pBuf;
	if(bQuit)
		::PostQuitMessage((int)msg.wParam);

	return bResult;
}

#endif	// _DEBUG