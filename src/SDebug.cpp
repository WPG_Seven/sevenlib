/************************************************************************\
* 文件：SDebug.cpp
*
* 说明：调试类实现
*
* 作者：Seven
\************************************************************************/ 

#ifdef _DEBUG

#include <crtdbg.h>
#include "../include/SDebug.h"
S_USE_SL()

#ifdef UNICODE
#define	CrtDbgReport _CrtDbgReportW
#else	// UNICODE
#define	CrtDbgReport _CrtDbgReport
#endif	// !UNICODE

void SDebug::Print(LPCTSTR pFormat, ...)
{
	if(!pFormat)
		return;

	// 不定参数
	TCHAR szBuf[S_MAXBUF] = {0};
	va_list args;
	va_start(args, pFormat);
#pragma warning(push)
#pragma warning(disable: 4996)
	_vsntprintf(szBuf, S_MAXBUF, pFormat, args);
	_tcscpy(szBuf + S_MAXBUF - 4, _T("..."));	// 超过缓冲长度时自动截断处理
#pragma warning(pop)
	va_end(args);

	// 输出到调试窗口
	::OutputDebugString(szBuf);
}

BOOL SDebug::Report(LPCTSTR pFileName, int nLine, ReportType type, LPCTSTR pFormat /* = NULL */, ...)
{
	TCHAR* pBuf = NULL;

	// 需要格式化输出时
	if(pFormat)
	{
		// 不定参数
		pBuf = new TCHAR[S_MAXBUF];
		memset(pBuf, 0, S_MAXBUF);
		va_list args;
		va_start(args, pFormat);
#pragma warning(push)
#pragma warning(disable: 4996)
		_vsntprintf(pBuf, S_MAXBUF, pFormat, args);
		_tcscpy(pBuf + S_MAXBUF - 4, _T("..."));		// 超过缓冲长度时自动截断处理
#pragma warning(pop)
		va_end(args);
	}

	// 防止消息队列中有 WM_QUIT 时不显示对话框
	MSG msg;
	BOOL bQuit = ::PeekMessage(&msg, NULL, WM_QUIT, WM_QUIT, PM_REMOVE);
	BOOL bResult = CrtDbgReport(type, pFileName, nLine, NULL, pBuf);
	if(pBuf)
		delete[] pBuf;
	if(bQuit)
		::PostQuitMessage((int)msg.wParam);

	return bResult;
}

#endif	// _DEBUG