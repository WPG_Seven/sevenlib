/************************************************************************\
* 文件：SAsynUdp.cpp
*
* 说明：异步UDP类实现
*
* 作者：Seven
\************************************************************************/

#include "../include/SSocket.h"
#include "../include/SCriticalSection.h"
#include "../include/SThread.h"
#include "../include/SAsynUdp.h"

S_USE_SL()

//////////////////////////////////////////////////////////////////////////
// SAsynUdp 类
S_BEGIN_PIMPL(SAsynUdp)
	class SAsynThread : public SThread
	{
	public:
		SAsynThread()
		{
			m_pAsynUdp = NULL;
			m_hWSAEvent = NULL;
		}

		BOOL Start(SAsynUdp* udp, WSAEVENT hEvent)
		{
			m_hWSAEvent = hEvent;
			m_pAsynUdp = udp;
			m_hSocket = m_pAsynUdp->GetSocket()->GetWinSock();

			// 绑定事件与SOCKET
			if(::WSAEventSelect(m_hSocket, m_hWSAEvent, FD_READ))
				return FALSE;

			return SThread::Start();
		}

		BOOL TryStop()
		{
			if(!SThread::TryStop())
				return FALSE;

			::WSASetEvent(m_hWSAEvent);
			return TRUE;
		}

	private:
		SAsynUdp* m_pAsynUdp;				// 异步UDP类
		SOCKET m_hSocket;
		WSAEVENT m_hWSAEvent;				// 绑定的事件
		WSANETWORKEVENTS m_NetWorkEvents;

	protected:
		virtual BOOL OnFrameRun()
		{
			// 等待网络事件
			if(WSA_WAIT_FAILED == ::WSAWaitForMultipleEvents(1, &m_hWSAEvent, FALSE, WSA_INFINITE, FALSE))
			{
				m_pAsynUdp->OnError();
				return FALSE;
			}
			// 得到事件类型
			if(::WSAEnumNetworkEvents(m_hSocket, m_hWSAEvent, &m_NetWorkEvents))
			{
				if(WSAEINPROGRESS == WSAGetLastError())
				{
					::WSAResetEvent(m_hWSAEvent);
					return TRUE;
				}
				m_pAsynUdp->OnError();
				return FALSE;
			}

			// 读取事件
			if((FD_READ & m_NetWorkEvents.lNetworkEvents) && !m_NetWorkEvents.iErrorCode[FD_READ_BIT])
				m_pAsynUdp->OnRecvFrom();

			return TRUE;
		}
	};

	SSocket m_Socket;		// 基础Socket
	WSAEVENT m_hWSAEvent;	// 事件
	SAsynThread s_Thread;	// 后台线程

	SCriticalSection m_CS;
	BYTE m_Buffer[SAsynUdp::MAX_BUFFER_SIZE];
	UINT m_uRecvLen;
	SIpEndpoint m_epRecvFrom;
	BOOL m_bRecvSuccess;
	BOOL m_bRet;

	void RecvFrom()
	{
		m_CS.Enter();
		m_bRecvSuccess = m_Socket.RecvFrom(m_Buffer, sizeof(m_Buffer), &m_epRecvFrom, &m_uRecvLen);
		m_CS.Leave();
	}

	BOOL RecvFrom(BYTE* buf, UINT buflen, SIpEndpoint* fromep, UINT* recvlen)
	{
		S_ASSERT(m_hWSAEvent != NULL);
		if(!m_hWSAEvent)
			return FALSE;

		m_CS.Enter();
		m_bRet = m_bRecvSuccess;
		if(m_bRet)
		{
			if(recvlen)
				*recvlen = m_uRecvLen;
			if(m_uRecvLen)
			{
				if(fromep)
					*fromep = m_epRecvFrom;

				if(buflen <= m_uRecvLen)
				{
					if(recvlen)
						*recvlen = buflen;
					memcpy(buf, m_Buffer, buflen);
				}
				else
				{
					memcpy(buf, m_Buffer, m_uRecvLen);
					memset(buf + m_uRecvLen, 0, buflen - m_uRecvLen);
				}

				m_uRecvLen = 0;
			}
		}
		m_CS.Leave();
		return m_bRet;
	}

S_END_PIMPL(SAsynUdp)

SAsynUdp::SAsynUdp() : S_INIT_PIMPL()
{
	m_PIMPL->m_hWSAEvent = NULL;
	m_PIMPL->m_bRecvSuccess = TRUE;
	m_PIMPL->m_uRecvLen = 0;
}

SAsynUdp::~SAsynUdp()
{
	Close();

	S_CLEAN_PIMPL();
}

void SAsynUdp::Close()
{
	if(m_PIMPL->s_Thread.TryStop())
		m_PIMPL->s_Thread.WaitStop();
	
	if(m_PIMPL->m_hWSAEvent)
	{
		::WSACloseEvent(m_PIMPL->m_hWSAEvent);
		m_PIMPL->m_hWSAEvent = NULL;
	}

	m_PIMPL->m_Socket.Close();
}

SSocket* SAsynUdp::GetSocket()const
{
	return &m_PIMPL->m_Socket;
}

BOOL SAsynUdp::Create(const SIpEndpoint& localep)
{
	// 创建基础UDP Socket
	if(!m_PIMPL->m_Socket.Create(SSocket::sockUDP, localep))
		return FALSE;

	// 创建事件
	m_PIMPL->m_hWSAEvent = ::WSACreateEvent();
	S_ASSERT(m_PIMPL->m_hWSAEvent != WSA_INVALID_EVENT);
	if(m_PIMPL->m_hWSAEvent == WSA_INVALID_EVENT)
	{
		m_PIMPL->m_hWSAEvent = NULL;
		Close();
		return FALSE;
	}

	// 开始运行后台线程
	if(!m_PIMPL->s_Thread.Start(this, m_PIMPL->m_hWSAEvent))
	{
		Close();
		return FALSE;
	}

	return TRUE;
}

void SAsynUdp::OnRecvFrom()
{
	m_PIMPL->RecvFrom();
}

void SAsynUdp::OnError()
{}

BOOL SAsynUdp::RecvFrom(BYTE* buf, UINT buflen,
					   SIpEndpoint* fromep /* = NULL */,
					   UINT* recvlen /* = NULL */)
{
	return m_PIMPL->RecvFrom(buf, buflen, fromep, recvlen);
}

BOOL SAsynUdp::SendTo(const BYTE* buf, UINT buflen, const SIpEndpoint& remoteep, UINT* sendlen /* = NULL */)
{
	return m_PIMPL->m_Socket.SendTo(buf, buflen, remoteep, sendlen);
}