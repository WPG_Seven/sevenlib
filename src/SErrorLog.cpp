/************************************************************************\
* 文件：SErrorInfo.cpp
*
* 说明：错误日志类类实现
*
* 作者：Seven
\************************************************************************/ 

#include <stdio.h>
#include "../include/SErrorLog.h"
S_USE_SL()

//// 管理FILE指针的内部类
class FilePointer
{
public:
	FilePointer() : m_pFile(NULL){}
	
	virtual ~FilePointer()
	{ if(m_pFile) ::fclose(m_pFile); }

	FILE* m_pFile;
};

//// 唯一实例
FilePointer g_fp;

//// 静态函数实现
BOOL SErrorLog::SetLogFile(LPCTSTR filename)
{
	if(g_fp.m_pFile)
		::fclose(g_fp.m_pFile);

	if(!filename)
	{
		g_fp.m_pFile = NULL;
		return TRUE;
	}

	::_tfopen_s(&g_fp.m_pFile, filename, _T("a"));

	if(!g_fp.m_pFile)
	{
		S_ASSERT(g_fp.m_pFile != NULL);
		return FALSE;
	}

	return TRUE;
}

void SErrorLog::Print(LPCTSTR pFormat, ...)
{
	if(!pFormat)
		return;

	// 不定参数
	TCHAR szBuf[S_MAXBUF] = { 0 };
	va_list args;
	va_start(args, pFormat);
#pragma warning(push)
#pragma warning(disable: 4996)
	_vsntprintf(szBuf, S_MAXBUF, pFormat, args);
	_tcscpy(szBuf + S_MAXBUF - 4, _T("..."));	// 超过缓冲长度时自动截断处理
#pragma warning(pop)
	va_end(args);

	// 写入的数据长度
	int datalen = _tcslen(szBuf) + 1;

	// 输出到日志文件
	S_VERIFY(::fwrite(szBuf, sizeof(TCHAR), datalen, g_fp.m_pFile) >= 0);

#ifdef _DEBUG
	// 输出到调试窗口
	::OutputDebugString(szBuf);
#endif
}
