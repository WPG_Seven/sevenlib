/************************************************************************\
* 文件：STimeSpan.cpp
*
* 说明：时间段类实现
*
* 作者：Seven
\************************************************************************/ 

#include "../include/SObject.h"
#include "../include/STimeSpan.h"
S_USE_SL()

S_BEGIN_PIMPL(STimeSpan)
	INT64 m_timeSpan;
S_END_PIMPL(STimeSpan)

STimeSpan::STimeSpan(INT64 Secs /* = 0 */) : S_INIT_PIMPL()
{
	m_PIMPL->m_timeSpan = Secs;
}

STimeSpan::STimeSpan(const STimeSpan& span) : S_INIT_PIMPL()
{
	m_PIMPL->m_timeSpan = span.m_PIMPL->m_timeSpan;
}

STimeSpan::~STimeSpan()
{
	S_CLEAN_PIMPL();
}

void STimeSpan::SetSpan(INT64 Secs, INT64 Mins /* = 0 */, INT64 Hours /* = 0 */, INT64 Days /* = 0 */)
{
	m_PIMPL->m_timeSpan = Secs + 60 * (Mins + 60 * (Hours + 24 * Days));
}

INT64 STimeSpan::GetTotalDays()const
{
	return (m_PIMPL->m_timeSpan / 86400);
}

INT64 STimeSpan::GetTotalHours()const
{
	return (m_PIMPL->m_timeSpan / 3600);
}

INT64 STimeSpan::GetTotalMinutes()const
{
	return (m_PIMPL->m_timeSpan / 60);
}

INT64 STimeSpan::GetTotalSeconds()const
{
	return m_PIMPL->m_timeSpan;
}

int STimeSpan::GetHours()const
{
	return static_cast<int>((m_PIMPL->m_timeSpan / 3600) - ((m_PIMPL->m_timeSpan / 86400) * 24));
}

int STimeSpan::GetMinutes()const
{
	return static_cast<int>((m_PIMPL->m_timeSpan / 60) - ((m_PIMPL->m_timeSpan / 3600) * 60));
}

int STimeSpan::GetSeconds()const
{
	return static_cast<int>(m_PIMPL->m_timeSpan - (m_PIMPL->m_timeSpan / 60) * 60);
}

STimeSpan STimeSpan::operator+(const STimeSpan& span)const
{
	return STimeSpan(m_PIMPL->m_timeSpan + span.m_PIMPL->m_timeSpan);
}

STimeSpan STimeSpan::operator-(const STimeSpan& span)const
{
	return STimeSpan(m_PIMPL->m_timeSpan - span.m_PIMPL->m_timeSpan);
}

STimeSpan& STimeSpan::operator+=(const STimeSpan& span)
{
	m_PIMPL->m_timeSpan += span.m_PIMPL->m_timeSpan;
	return *this;
}

STimeSpan& STimeSpan::operator-=(const STimeSpan& span)
{
	m_PIMPL->m_timeSpan -= span.m_PIMPL->m_timeSpan;
	return *this;
}

STimeSpan& STimeSpan::operator=(const STimeSpan& span)
{
	m_PIMPL->m_timeSpan = span.m_PIMPL->m_timeSpan;
	return *this;
}

bool STimeSpan::operator==(const STimeSpan& span)const
{
	return (m_PIMPL->m_timeSpan == span.m_PIMPL->m_timeSpan);
}

bool STimeSpan::operator!=(const STimeSpan& span)const
{
	return (m_PIMPL->m_timeSpan != span.m_PIMPL->m_timeSpan);
}

bool STimeSpan::operator<(const STimeSpan& span)const
{
	return (m_PIMPL->m_timeSpan < span.m_PIMPL->m_timeSpan);
}

bool STimeSpan::operator>(const STimeSpan& span)const
{
	return (m_PIMPL->m_timeSpan > span.m_PIMPL->m_timeSpan);
}

bool STimeSpan::operator<=(const STimeSpan& span)const
{
	return (m_PIMPL->m_timeSpan <= span.m_PIMPL->m_timeSpan);
}

bool STimeSpan::operator>=(const STimeSpan& span)const
{
	return (m_PIMPL->m_timeSpan >= span.m_PIMPL->m_timeSpan);
}