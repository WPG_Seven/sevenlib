/************************************************************************\
* 文件：SSocket.cpp
*
* 说明：基础套接字相关类实现
*
* 作者：Seven
\************************************************************************/

#include <atlconv.h>
#include "../include/SCriticalSection.h"
#include "../include/SSocket.h"

#pragma comment(lib, "WS2_32.lib")

S_USE_SL()

//////////////////////////////////////////////////////////////////////////
// SIpEndPoint 类
S_BEGIN_PIMPL(SIpEndpoint)
SOCKADDR_IN m_Addr;
S_END_PIMPL(SIpEndpoint)

SIpEndpoint::SIpEndpoint(USHORT port /* = 0 */, LPCTSTR ip /* = NULL */) : S_INIT_PIMPL()
{
	memset(&(m_PIMPL->m_Addr), 0, sizeof(SOCKADDR_IN));

	m_PIMPL->m_Addr.sin_family = AF_INET;

	if(!ip)
		m_PIMPL->m_Addr.sin_addr.S_un.S_addr = ::htonl(INADDR_ANY);
	else
	{
		USES_CONVERSION;
		const char* str_addr = T2A((LPTSTR)ip);
		unsigned long ipad = inet_addr(str_addr);
		if(INADDR_NONE == ipad)
			m_PIMPL->m_Addr.sin_addr.S_un.S_addr = ::htonl(INADDR_ANY);
		else
			m_PIMPL->m_Addr.sin_addr.S_un.S_addr = ipad;
	}

	m_PIMPL->m_Addr.sin_port = ::htons(port);
}

SIpEndpoint::SIpEndpoint(const SIpEndpoint& ep) : S_INIT_PIMPL()
{
	memcpy(&(m_PIMPL->m_Addr), &(ep.m_PIMPL->m_Addr), sizeof(SOCKADDR_IN));
}

SIpEndpoint::~SIpEndpoint()
{
	S_CLEAN_PIMPL();
}

USHORT SIpEndpoint::GetPort()const
{
	return ::ntohs(m_PIMPL->m_Addr.sin_port);
}

LPCTSTR SIpEndpoint::GetAddr()const
{
	USES_CONVERSION;
	return A2T(inet_ntoa(m_PIMPL->m_Addr.sin_addr));
}

BOOL SIpEndpoint::SetAddr(LPCTSTR ip)
{
	if(!ip)
		m_PIMPL->m_Addr.sin_addr.S_un.S_addr = ::htonl(INADDR_ANY);
	else
	{
		USES_CONVERSION;
		const char* str_addr = T2A((LPTSTR)ip);
		unsigned long ipad = inet_addr(str_addr);
		if(INADDR_NONE == ipad)
		{
			m_PIMPL->m_Addr.sin_addr.S_un.S_addr = ::htonl(INADDR_ANY);
			return FALSE;
		}
		else
			m_PIMPL->m_Addr.sin_addr.S_un.S_addr = ipad;
	}

	return TRUE;
}

BOOL SIpEndpoint::SetPort(USHORT port)
{
	m_PIMPL->m_Addr.sin_port = ::htons(port);
	return TRUE;
}

bool SIpEndpoint::operator==(const SIpEndpoint& ep)
{
	return m_PIMPL->m_Addr.sin_addr.S_un.S_addr == ep.m_PIMPL->m_Addr.sin_addr.S_un.S_addr &&
		m_PIMPL->m_Addr.sin_port == ep.m_PIMPL->m_Addr.sin_port;
}

bool SIpEndpoint::operator!=(const SIpEndpoint& ep)
{
	return m_PIMPL->m_Addr.sin_addr.S_un.S_addr != ep.m_PIMPL->m_Addr.sin_addr.S_un.S_addr ||
		m_PIMPL->m_Addr.sin_port != ep.m_PIMPL->m_Addr.sin_port;
}

SIpEndpoint& SIpEndpoint::operator=(const SIpEndpoint& ep)
{
	memcpy(&(m_PIMPL->m_Addr), &(ep.m_PIMPL->m_Addr), sizeof(SOCKADDR_IN));
	return *this;
}



//////////////////////////////////////////////////////////////////////////
// SIpEndPoint 类
S_BEGIN_PIMPL(SSocket)

public:
	SOCKET m_socket;
	int m_nSendRet;
	int m_nRecvRet;

	// 检查并初始化网络
	void CheckAndInitWSA()
	{
		s_CS.Enter();
		if(s_uRef)
			++ s_uRef;
		else
		{
			WSADATA wsaData;
			if(!::WSAStartup(MAKEWORD(2, 1), &wsaData))
				++ s_uRef;
		}
		s_CS.Leave();
	}

	// 清理网络
	void CheckAndCleanWSA()
	{
		s_CS.Enter();
		if(s_uRef)
		{
			-- s_uRef;
			if(!s_uRef)
				::WSACleanup();
		}
		s_CS.Leave();
	}

	// 内部实现
private:
	static UINT s_uRef;
	static SCriticalSection s_CS;


S_END_PIMPL(SSocket)

// 静态变量初始化
UINT SSocket::S_PIMPL(SSocket)::s_uRef = 0;
SCriticalSection SSocket::S_PIMPL(SSocket)::s_CS;

SSocket::SSocket() : S_INIT_PIMPL()
{
	m_PIMPL->m_socket = NULL;
	
	m_PIMPL->CheckAndInitWSA();
}

SSocket::~SSocket()
{
	Close();

	m_PIMPL->CheckAndCleanWSA();
	S_CLEAN_PIMPL();
}

void SSocket::Close()
{
	if(m_PIMPL->m_socket)
	{
		::closesocket(m_PIMPL->m_socket);
		m_PIMPL->m_socket = NULL;
	}
}

SOCKET SSocket::GetWinSock()const
{
	return m_PIMPL->m_socket;
}

BOOL SSocket::Create(SockType type, const SIpEndpoint& localep)
{
	S_ASSERT(m_PIMPL->m_socket == NULL);
	if(m_PIMPL->m_socket)
		return FALSE;

	int tp;
	switch(type)
	{
		case sockTCP: tp = SOCK_STREAM; break;
		case sockUDP: tp = SOCK_DGRAM; break;
		default: return FALSE;
	}
	m_PIMPL->m_socket = ::socket(AF_INET, tp, 0);
	S_ASSERT(m_PIMPL->m_socket != INVALID_SOCKET);
	if(INVALID_SOCKET == m_PIMPL->m_socket)
	{
		m_PIMPL->m_socket = NULL;
		return FALSE;
	}

	if(::bind(m_PIMPL->m_socket, (SOCKADDR*)(&localep.m_PIMPL->m_Addr), sizeof(SOCKADDR_IN)))
	{
		Close();
		return FALSE;
	}

	return TRUE;
}

BOOL SSocket::Listen(int maxconn /* = SOMAXCONN */)
{
	S_ASSERT(m_PIMPL->m_socket != NULL);
	return !::listen(m_PIMPL->m_socket, maxconn);
}

BOOL SSocket::Accpet(SSocket& clientsock, SIpEndpoint* clientep /* = NULL */)
{
	S_ASSERT(m_PIMPL->m_socket != NULL);
	clientsock.Close();
	if(clientep)
	{
		int len = sizeof(SOCKADDR_IN);
		clientsock.m_PIMPL->m_socket = 
			::accept(m_PIMPL->m_socket, (SOCKADDR*)(&(clientep->m_PIMPL->m_Addr)), &len);
	}
	else
	{
		clientsock.m_PIMPL->m_socket = ::accept(m_PIMPL->m_socket, NULL, NULL);
	}
	S_ASSERT(clientsock.m_PIMPL->m_socket != INVALID_SOCKET);
	if(clientsock.m_PIMPL->m_socket == INVALID_SOCKET)
	{
		clientsock.m_PIMPL->m_socket = NULL;
		return FALSE;
	}

	return TRUE;
}

BOOL SSocket::Connect(const SIpEndpoint& remoteep)
{
	S_ASSERT(m_PIMPL->m_socket != NULL);
	return !::connect(m_PIMPL->m_socket, (SOCKADDR*)(&remoteep.m_PIMPL->m_Addr), sizeof(SOCKADDR_IN));
}

BOOL SSocket::Send(const BYTE* buf, UINT buflen, UINT* sendlen /* = NULL */)
{
	S_ASSERT(m_PIMPL->m_socket != NULL);
	m_PIMPL->m_nSendRet = ::send(m_PIMPL->m_socket, (const char*)buf, buflen, 0);
	if(SOCKET_ERROR == m_PIMPL->m_nSendRet)
		return FALSE;
	
	if(sendlen)
		*sendlen = m_PIMPL->m_nSendRet;

	return TRUE;
}

BOOL SSocket::SendTo(const BYTE* buf, UINT buflen, const SIpEndpoint& remoteep, UINT* sendlen /* = NULL */)
{
	S_ASSERT(m_PIMPL->m_socket != NULL);
	m_PIMPL->m_nSendRet = ::sendto(m_PIMPL->m_socket,
								   (const char*)buf, buflen, 0,
								   (SOCKADDR*)(&remoteep.m_PIMPL->m_Addr),
								   sizeof(SOCKADDR_IN));
	if(SOCKET_ERROR == m_PIMPL->m_nSendRet)
		return FALSE;
	
	if(sendlen)
		*sendlen = m_PIMPL->m_nSendRet;
	
	return TRUE;
}

BOOL SSocket::Recv(BYTE* buf, UINT buflen, UINT* recvlen /* = NULL */)
{
	S_ASSERT(m_PIMPL->m_socket != NULL);
	m_PIMPL->m_nRecvRet = ::recv(m_PIMPL->m_socket, (char*)buf, buflen, 0);
	if(SOCKET_ERROR == m_PIMPL->m_nRecvRet)
		return FALSE;
	
	if(recvlen)
		*recvlen = m_PIMPL->m_nRecvRet;

	return TRUE;
}

BOOL SSocket::RecvFrom(BYTE* buf, UINT buflen,
					   SIpEndpoint* fromep /* = NULL */,
					   UINT* recvlen /* = NULL */)
{
	S_ASSERT(m_PIMPL->m_socket != NULL);
	if(fromep)
	{
		int len = sizeof(SOCKADDR_IN);
		m_PIMPL->m_nRecvRet = ::recvfrom(m_PIMPL->m_socket, (char*)buf, buflen, 0,
										 (SOCKADDR*)(&(fromep->m_PIMPL->m_Addr)), &len);
	}
	else
		m_PIMPL->m_nRecvRet = ::recvfrom(m_PIMPL->m_socket, (char*)buf, buflen, 0, NULL, NULL);
	
	if(SOCKET_ERROR == m_PIMPL->m_nRecvRet)
		return FALSE;

	if(recvlen)
		*recvlen = m_PIMPL->m_nRecvRet;

	return TRUE;
}

