/************************************************************************\
* 文件：SEvent.cpp
*
* 说明：多线程同步-事件类实现
*
* 作者：Seven
\************************************************************************/

#include "../include/SEvent.h"
S_USE_SL()

S_BEGIN_PIMPL(SEvent)
HANDLE	m_hEvent;
S_END_PIMPL(SEvent)

SEvent::SEvent() : S_INIT_PIMPL()
{
	m_PIMPL->m_hEvent = NULL;
}

SEvent::~SEvent()
{
	Close();

	S_CLEAN_PIMPL();
}

BOOL SEvent::Set()
{
	S_ASSERT(m_PIMPL->m_hEvent != NULL);

	return ::SetEvent(m_PIMPL->m_hEvent);
}

BOOL SEvent::Pulse()
{
	S_ASSERT(m_PIMPL->m_hEvent != NULL);

	return ::PulseEvent(m_PIMPL->m_hEvent);
}

BOOL SEvent::Reset()
{
	S_ASSERT(m_PIMPL->m_hEvent != NULL);

	return ::ResetEvent(m_PIMPL->m_hEvent);
}

BOOL SEvent::Wait(DWORD timeout /* = INFINITE */, S_OUT DWORD* result /* = NULL */)
{
	S_ASSERT(m_PIMPL->m_hEvent != NULL);

	DWORD rt = ::WaitForSingleObject(m_PIMPL->m_hEvent, timeout);
	if(result)
		*result = rt;
	return WAIT_FAILED == rt ? FALSE : TRUE;
}

void SEvent::Close()
{
	if(m_PIMPL->m_hEvent)
	{
		::CloseHandle(m_PIMPL->m_hEvent);
		m_PIMPL->m_hEvent = NULL;
	}
}

BOOL SEvent::Create(BOOL manualreset, BOOL initstate, LPCTSTR name /* = NULL */, LPSECURITY_ATTRIBUTES attr /* = NULL */)
{
	S_ASSERT(m_PIMPL->m_hEvent == NULL);
	if(m_PIMPL->m_hEvent)
		return FALSE;

	m_PIMPL->m_hEvent = ::CreateEvent(attr, manualreset, initstate, name);
	S_ASSERT(m_PIMPL->m_hEvent != NULL);
	return m_PIMPL->m_hEvent ? TRUE : FALSE;
}

BOOL SEvent::Open(LPCTSTR name)
{
	S_ASSERT(m_PIMPL->m_hEvent == NULL);
	if(m_PIMPL->m_hEvent)
		return FALSE;

	m_PIMPL->m_hEvent = ::OpenEvent(EVENT_ALL_ACCESS, FALSE, name);
	S_ASSERT(m_PIMPL->m_hEvent != NULL);
	return m_PIMPL->m_hEvent ? TRUE : FALSE;
}

HANDLE SEvent::GetHandle()const
{
	return m_PIMPL->m_hEvent;
}