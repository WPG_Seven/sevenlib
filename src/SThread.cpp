/************************************************************************\
* 文件：SThread.cpp
*
* 说明：线程类实现
*
* 作者：Seven
\************************************************************************/

#include <process.h>
#include "../include/SCriticalSection.h"
#include "../include/SThread.h"

S_USE_SL()

S_BEGIN_PIMPL(SThread)

	HANDLE	m_hThread;
	BOOL	m_bInherit;
	DWORD	m_dwStackSize;
	BOOL	m_bShoudRun;
	UINT	m_nID;
	BOOL	m_bSuspend;
	SCriticalSection m_CS;

	// 线程通用入口
	static unsigned int __stdcall ThreadFunc(void* args)
	{
		SThread* th = (SThread*)args;
		while(th->m_PIMPL->ShouldRun())
		{
			// 帧函数返回FALSE，则结束线程
			if(!th->OnFrameRun())
			{
				th->m_PIMPL->m_CS.Enter();
				th->m_PIMPL->m_bShoudRun = FALSE;
				th->m_PIMPL->m_CS.Leave();
				break;
			}
		}

		// 结束时调用
		th->OnStop();

		return 0;
	}

	BOOL ShouldRun()
	{
		m_CS.Enter();
		BOOL ret = m_bShoudRun;
		m_CS.Leave();
		return ret;
	}

S_END_PIMPL(SThread)

SThread::SThread(BOOL bSuspend /* = FALSE */,
				 BOOL bInherit /* = FALSE */,
				 DWORD dwStackSize /* = 0 */) : S_INIT_PIMPL()
{
	m_PIMPL->m_hThread = NULL;
	m_PIMPL->m_bInherit = bInherit;
	m_PIMPL->m_dwStackSize = dwStackSize;
	m_PIMPL->m_bSuspend = bSuspend;
	m_PIMPL->m_nID = 0;
}

SThread::~SThread()
{
	TryStop();
	WaitStop();

	S_CLEAN_PIMPL();
}

BOOL SThread::Start()
{
	S_ASSERT(m_PIMPL->m_hThread == NULL);
	if(m_PIMPL->m_hThread)
		return FALSE;

	// 是否能被子进程继承
	SECURITY_ATTRIBUTES sa;
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = m_PIMPL->m_bInherit;

	// 开始标志
	m_PIMPL->m_bShoudRun = TRUE;

	// 创建线程
	m_PIMPL->m_hThread = (HANDLE)_beginthreadex(&sa, m_PIMPL->m_dwStackSize,
												&S_PIMPL(SThread)::ThreadFunc, this,
												m_PIMPL->m_bSuspend ? CREATE_SUSPENDED : 0,
												&m_PIMPL->m_nID);
	S_ASSERT(m_PIMPL->m_hThread != NULL);

	return m_PIMPL->m_hThread ? TRUE : FALSE;
}

VOID SThread::OnStop()
{}

HANDLE SThread::GetHandle()const
{
	return m_PIMPL->m_hThread;
}

UINT SThread::GetID()const
{
	return m_PIMPL->m_nID;
}

BOOL SThread::Suspend(DWORD* dwPreSuspendCount /* = NULL */)
{
	S_ASSERT(m_PIMPL->m_hThread != NULL);
	DWORD ret = ::SuspendThread(m_PIMPL->m_hThread);
	if(-1 == ret)
		return FALSE;
	if(dwPreSuspendCount)
		*dwPreSuspendCount = ret;
	return TRUE;
}

BOOL SThread::Resume(DWORD* dwPreSuspendCount /* = NULL */)
{
	S_ASSERT(m_PIMPL->m_hThread != NULL);
	DWORD ret = ::ResumeThread(m_PIMPL->m_hThread);
	if(-1 == ret)
		return FALSE;
	if(dwPreSuspendCount)
		*dwPreSuspendCount = ret;
	return TRUE;
}

BOOL SThread::IsRunning()const
{
	if(!m_PIMPL->m_hThread)
		return FALSE;

	DWORD dwResult = WaitForSingleObject(m_PIMPL->m_hThread, 0);
	if(WAIT_OBJECT_0 == dwResult)
		return FALSE;
	else if(WAIT_TIMEOUT == dwResult)
		return TRUE;
	
	return FALSE;
}

BOOL SThread::WaitStop()
{
	if(!m_PIMPL->m_hThread)
		return FALSE;

	S_TRACE(_T("Waiting thread stop...\n"));
	DWORD retwait = ::WaitForSingleObject(m_PIMPL->m_hThread, INFINITE);

	if(WAIT_OBJECT_0 != retwait)
		return FALSE;

	if(!::CloseHandle(m_PIMPL->m_hThread))
		return FALSE;

	m_PIMPL->m_nID = 0;
	m_PIMPL->m_hThread = NULL;
	return TRUE;
}

BOOL SThread::TryStop()
{
	if(!m_PIMPL->m_hThread)
		return FALSE;

	m_PIMPL->m_CS.Enter();
	m_PIMPL->m_bShoudRun = FALSE;
	m_PIMPL->m_CS.Leave();
	return TRUE;
}