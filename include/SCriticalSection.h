/************************************************************************\
* 文件：SCriticalSection.h
*
* 说明：多线程同步-关键区类
*
* 作者：Seven
\************************************************************************/

#pragma once

#include "CommonDef.h"
S_BEGIN_SL()

class S_EXPORT SCriticalSection
{
	S_DECLARE_PIMPL(SCriticalSection)
	S_DISABLE_COPY(SCriticalSection)

public:
	SCriticalSection();
	virtual ~SCriticalSection();

	// 进入关键区
	void Enter();
	
	// 离开关键区
	void Leave();
	
	// 内部实现的接口
	CRITICAL_SECTION* GetCriticalSection()const;
};

S_END_SL()
