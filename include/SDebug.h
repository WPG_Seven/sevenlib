/************************************************************************\
* 文件：SDbug.h
*
* 说明：调试输出与中断相关
*
* 作者：Seven
\************************************************************************/

#pragma once

#ifdef _DEBUG

#include "CommonDef.h"
S_BEGIN_SL()

class S_EXPORT SDebug
{
	S_DISABLE_CONSTRUCT(SDebug)

public:
	// 中断消息框的类型
	enum ReportType
	{
		rptWarn,
		rptError,
		rptAssert,
		rptErrcnt,
	};

	// 调试窗口输出
	static void Print(LPCTSTR pFormat, ...);

	// 中断程序，并弹出消息框
	static BOOL Report(LPCTSTR pFileName, int nLine, ReportType type, LPCTSTR pFormat = NULL, ...);
};

S_END_SL()

//// 调试宏
// 断言
#define S_ASSERT(f) ((void) ((f) || SDebug::Report(_T(__FILE__), __LINE__, SDebug::rptAssert, _T(#f)) || (__debugbreak(), 0)))

// 验证
#define S_VERIFY(f) S_ASSERT(f)

// 跟踪输出
#define S_TRACE SDebug::Print

#else	// _DEBUG

#define S_ASSERT(f)	((void)0)
#define S_VERIFY(f)	((void)(f))
#define S_TRACE		__noop

#endif	// !_DEBUG