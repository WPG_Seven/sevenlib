/************************************************************************\
* 文件：SErrorLog.h
*
* 说明：错误日志类，对发生的错误进行输出与查询
*
* 作者：Seven
\************************************************************************/ 

#pragma once
#include "CommonDef.h"
S_BEGIN_SL()

class S_EXPORT SErrorLog
{
	S_DISABLE_CONSTRUCT(SErrorLog)

public:
	// 设定日志输出的文件
	static BOOL SetLogFile(S_OPT LPCTSTR filename);

	// 日志输出
	static void Print(LPCTSTR pFormat, ...);
};

S_END_SL()