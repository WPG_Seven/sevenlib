/************************************************************************\
* 文件：SStreamObject.h
*
* 说明：流对象基类
*
* 作者：Seven
\************************************************************************/ 

#pragma once
#include "CommonDef.h"
S_BEGIN_SL()

class S_EXPORT SStreamObject : public SObject
{
public:
	//// 构造
	SStreamObject();
	virtual ~SStreamObject();
	
	// 读取
	virtual BOOL Read(S_OUT void* pData, DWORD datalenBytes, S_OUT DWORD* pReadLen = NULL) = 0;

	// 写入
	virtual BOOL Write(const void* pData, DWORD datalenBytes, S_OUT DWORD* pWriteLen = NULL) = 0;
};

S_END_SL()