/************************************************************************\
* 文件：SThread.h
*
* 说明：线程类
*
* 作者：Seven
\************************************************************************/

#pragma once

#include "CommonDef.h"
S_BEGIN_SL()

class S_EXPORT SThread
{
	S_DECLARE_PIMPL(SThread)
	S_DISABLE_COPY(SThread)

public:
	SThread(BOOL bSuspend = FALSE, BOOL bInherit = FALSE, DWORD dwStackSize = 0);
	virtual ~SThread();

	// 开始运行线程
	BOOL Start();

	// 尝试结束线程，不会立即结束，可以再调用WaitStop等待线程完全结束
	BOOL TryStop();

	// 等待线程结束
	BOOL WaitStop();

	// 挂起
	BOOL Suspend(DWORD* dwPreSuspendCount = NULL);

	// 恢复
	BOOL Resume(DWORD* dwPreSuspendCount = NULL);

	// 是否正在运行
	BOOL IsRunning()const;

	// 得到线程句柄
	HANDLE GetHandle()const;

	// 得到线程ID
	UINT GetID()const;

protected:
	// 用于重写的线程帧函数，线程会不断循环运行帧函数，返回FALSE停止运行
	virtual BOOL OnFrameRun() = 0;

	// 线程结束时调用
	virtual void OnStop();
};

S_END_SL()
