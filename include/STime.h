/************************************************************************\
* 文件：STime.h
*
* 说明：时间类，表示某个时间点
*
* 作者：Seven
\************************************************************************/ 

#pragma once
#include "CommonDef.h"
#include "STimeSpan.h"
S_BEGIN_SL()

class STimeSpan;
class S_EXPORT STime
{
	S_DECLARE_PIMPL(STime)

public:
	//// 构造
	STime(INT64 time = 0);	// GMT时间为 time
	STime(const STime& point);
	virtual ~STime();
	
	//// 操作
	// 设置到当前时间
	BOOL CurrentTime();

	// 设置时间，nDst 是否使用夏令时，1 - 使用，0 - 不使用，-1 - 系统决定
	BOOL SetTime(INT64 time);
	BOOL SetTime(int nYear, int nMonth, int nDay, int nHour, int nMin, int nSec, int nDST = -1);
	BOOL SetTime(const SYSTEMTIME& st, int nDST = -1);
	BOOL SetTime(const tm& ttm);
	BOOL SetTime(const FILETIME& ft, int nDST = -1);		// 使用文件时间结构

	//// 属性
	// 是否有效
	BOOL IsValid()const;

	// 得到时间
	INT64	GetTime()const;						// GMT 时间
	BOOL	GetTime(S_OUT SYSTEMTIME& st)const;	// 系统时间
	BOOL	GetTime(S_OUT tm& ttm)const;		// TM 结构时间
	BOOL	GetTime(S_OUT FILETIME& ft)const;	// FILETIME 结构
	int GetYear()const;				// [1970, ...] error: -1
	int GetMonth()const;			// [1, 12] error: -1
	int GetDay()const;				// [1, 31] error: -1
	int GetHour()const;				// [0, 23] error: -1
	int GetMinute()const;			// [0, 59] error: -1
	int GetSecond()const;			// [0, 59] error: -1
	int GetDayOfWeek()const;		// [0, 6] 0 is Sunday, error: -1
	int GetDayOfYear()const;		// [0, 365] error: -1
	
	//// 重载运算符
	STime& operator=(const STime& point);
	STime& operator+=(const STimeSpan& span);
	STime& operator-=(const STimeSpan& span);
	STime operator+(const STimeSpan& span)const;
	STime operator-(const STimeSpan& span)const;
	STimeSpan operator-(const STime& point)const;
	bool operator==(const STime& point)const;
	bool operator!=(const STime& point)const;
	bool operator<(const STime& point)const;
	bool operator>(const STime& point)const;
	bool operator<=(const STime& point)const;
	bool operator>=(const STime& point)const;
};

S_END_SL()