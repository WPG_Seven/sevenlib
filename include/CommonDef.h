/************************************************************************\
* 文件：CommonDef.h
*
* 说明：一些公共内容的定义
*
* 作者：Seven
\************************************************************************/ 

#pragma once

// 公共头文件
#include <winsock2.h>
#include <windows.h>
#include <stdio.h>
#include <time.h>
#include <tchar.h>

// 导出函数调用方式
#define S_API __stdcall

// 导入/导出符号
#ifdef	SEVENLIB_EXPORTS
	#define S_EXPORT	__declspec(dllexport)
#else
	#define S_EXPORT	__declspec(dllimport)
#endif

// SevenLib 命名空间
#define S_SL			SevenLib
#define S_BEGIN_SL()	namespace S_SL{
#define S_END_SL()		}
#define S_USE_SL()		using namespace S_SL;

//// 指针和引用参数的说明
#define S_IN	// 往内传递，即函数内部使用原始参数，但不会改变参数结构
#define S_OUT	// 往外传递，即函数内部不使用原始参数，但会改变其结构并传出
#define S_OPT	// 可选参数，可以为 0 或 NULL 的参数

// 通用临时缓冲的大小(数量，不是字节)
#define S_MAXBUF	512

// 禁止拷贝
#define S_DISABLE_COPY(class_name)\
private:\
	class_name(const class_name&);\
	class_name& operator=(const class_name& src);

// 禁止用构造函数生成实例
#define S_DISABLE_CONSTRUCT(class_name)\
private:\
	class_name();\


//// PIMPL功能
// PIMPL类名称
#define S_PIMPL(class_name) class_name##_Impl

// PIMPL初始化
#define S_INIT_PIMPL() m_PIMPL(CreatePIMPL(this))

// PIMPL清理
#define S_CLEAN_PIMPL() delete m_PIMPL;

// 基本功能 声明宏
#define S_DECLARE_PIMPL(class_name)\
private:\
	class S_PIMPL(class_name);\
	static S_PIMPL(class_name)* CreatePIMPL(const class_name* pPannel);\
	S_PIMPL(class_name)* const m_PIMPL;

// PIMPL列表
#define S_BEGIN_PIMPL(class_name)\
class class_name::S_PIMPL(class_name)\
{\
private:\
	const class_name* const m_PANNEL;\
public:\
	S_PIMPL(class_name)(const class_name* pPannel) : m_PANNEL(pPannel){}

#define S_END_PIMPL(class_name) };\
	class_name::S_PIMPL(class_name)* class_name::CreatePIMPL(const class_name* pPannel)\
	{ return new S_PIMPL(class_name)(pPannel); }


#include "SDebug.h"