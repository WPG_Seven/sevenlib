/************************************************************************\
* 文件：SSocket.h
*
* 说明：基础套接字相关类
*
* 作者：Seven
\************************************************************************/

#pragma once

#include "CommonDef.h"
S_BEGIN_SL()

//////////////////////////////////////////////////////////////////////////
// SIpEndpoint 类
class S_EXPORT SIpEndpoint
{
	S_DECLARE_PIMPL(SIpEndpoint);

public:
	SIpEndpoint(USHORT port = 0, LPCTSTR ip = NULL);
	SIpEndpoint(const SIpEndpoint& ep);
	virtual ~SIpEndpoint();

	// 得到端口
	USHORT GetPort()const;

	// 得到IP地址
	LPCTSTR GetAddr()const;
	
	// 设置地址
	BOOL SetAddr(LPCTSTR ip);

	// 设置端口
	BOOL SetPort(USHORT port);

	//// 操作符重载
	bool operator==(const SIpEndpoint& ep);
	bool operator!=(const SIpEndpoint& ep);
	SIpEndpoint& operator=(const SIpEndpoint& ep);

	// 友元
	friend class SSocket;
};


//////////////////////////////////////////////////////////////////////////
// SSocket 类
class S_EXPORT SSocket
{
	S_DECLARE_PIMPL(SSocket);
	S_DISABLE_COPY(SSocket);

public:
	// Socket类型
	enum SockType
	{
		sockTCP,	// TCP连接
		sockUDP,	// UDP连接
	};

	SSocket();
	virtual ~SSocket();

	// 得到Socket句柄
	SOCKET GetWinSock()const;

	// 创建
	BOOL Create(SockType type, const SIpEndpoint& localep);

	// 关闭socket
	void Close();

	// 监听
	BOOL Listen(int maxconn = SOMAXCONN);

	// 接受请求
	BOOL Accpet(S_OUT SSocket& clientsock, S_OUT SIpEndpoint* clientep = NULL);

	// 连接/绑定远程主机
	BOOL Connect(const SIpEndpoint& remoteep);

	// 发送
	BOOL Send(const BYTE* buf, UINT buflen, S_OUT UINT* sendlen = NULL);
	BOOL SendTo(const BYTE* buf, UINT buflen,
				const SIpEndpoint& remoteep,
				S_OUT UINT* sendlen = NULL);

	// 接收
	BOOL Recv(S_OUT BYTE* buf, UINT buflen, S_OUT UINT* recvlen = NULL);
	BOOL RecvFrom(S_OUT BYTE* buf, UINT buflen,
				  S_OUT SIpEndpoint* fromep = NULL,
				  S_OUT UINT* recvlen = NULL);
};

S_END_SL()