/************************************************************************\
* 文件：SEvent.h
*
* 说明：多线程同步-事件类
*
* 作者：Seven
\************************************************************************/

#pragma once

#include "CommonDef.h"
S_BEGIN_SL()

class S_EXPORT SEvent
{
	S_DECLARE_PIMPL(SEvent)
	S_DISABLE_COPY(SEvent)

public:
	SEvent();
	virtual ~SEvent();

	// 创建事件
	BOOL Create(BOOL manualreset, BOOL initstate,
					 LPCTSTR name = NULL, LPSECURITY_ATTRIBUTES attr = NULL);

	// 打开事件
	BOOL Open(LPCTSTR name);

	// 关闭事件
	void Close();

	// 设置事件为有信号状态
	BOOL Set();

	// 设置事件为有信号状态，手动重置时，所有等待线程都将进入活动状态，然后重置事件
	BOOL Pulse();

	// 手动重置事件为无信号状态
	BOOL Reset();

	// 等待当前事件，直到其为有信号状态
	BOOL Wait(DWORD timeout = INFINITE, S_OUT DWORD* result = NULL);

	// 得到内部句柄
	HANDLE GetHandle()const;
};

S_END_SL()