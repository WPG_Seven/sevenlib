/************************************************************************\
* 文件：SObjectAutoPtr.h
*
* 说明：自动指针类，会自动管理指向的SObject系对象的内存
*
* 作者：Seven
\************************************************************************/ 

#pragma once
#include "CommonDef.h"
S_BEGIN_SL()

class S_EXPORT SObjectAutoPtr : public SObject
{
	S_DECLARE_PIMPL(SObjectAutoPtr)

public:
	SObjectAutoPtr(SObject* ptr = NULL);
	SObjectAutoPtr(const SObjectAutoPtr& ptr);
	virtual ~SObjectAutoPtr();

	//// 操作
	// 清空指针，会执行清理
	void Delete();

	// 脱离指针，不执行清理
	SObject* Detach();

	//// 属性
	// 是否有效(空指针为无效)
	virtual BOOL IsValid()const;

	// 得到 SObject 指针
	SObject* GetObject()const;

	//// 操作符
	operator SObject*()const;
	SObjectAutoPtr& operator=(const SObjectAutoPtr& ptr);
};

S_END_SL()