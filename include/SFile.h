/************************************************************************\
* 文件：SFile.h
*
* 说明：通用文件类
*
* 作者：Seven
\************************************************************************/ 

#pragma once
#include "CommonDef.h"
#include "STime.h"
S_BEGIN_SL()

class S_EXPORT SFile
{
	S_DECLARE_PIMPL(SFile)
	S_DISABLE_COPY(SFile)

public:
	//// 定义
	// 文件名最大长度
	static const UINT MAX_NAMEBUF = MAX_PATH;

	// 访问权限，可combine
	enum AccessMode
	{
		accessNone		= 0x00,				// 只允许获取与设备文件相关的信息
		accessRead		= GENERIC_READ,		// 读权限
		accessWrite		= GENERIC_WRITE,	// 写权限
		accessExecute	= GENERIC_EXECUTE,	// 执行权限
		accessAll		= GENERIC_ALL,		// 所有权限
	};

	// 共享方式，可combine
	enum ShareMode
	{
		shareNone	= 0x00,					// 不共享
		shareRead	= FILE_SHARE_READ,		// 共享读权限
		shareWrite	= FILE_SHARE_WRITE,		// 共享写权限
		shareDelete	= FILE_SHARE_DELETE,	// 共享删除权限
		shareAll	= FILE_SHARE_READ | FILE_SHARE_READ | FILE_SHARE_DELETE,
	};

	// 文件打开时的特性，可combine，FFLAG_RANDOM_ACCESS 与 FFLAG_SEQUENTIAL_SCAN 互斥
	enum FileFlag
	{
		flagNormal			= 0x00,							// 一般特性
		flagBackupSemantics = FILE_FLAG_BACKUP_SEMANTICS,	// 打开目录时使用
		flagNoBuffering		= FILE_FLAG_NO_BUFFERING,		// 禁止对文件进行缓冲处理，只能写入磁盘卷的扇区块
		flagWriteThrough	= FILE_FLAG_WRITE_THROUGH,		// 操作系统不得推迟对文件的写操作
		flagRandomAccess	= FILE_FLAG_RANDOM_ACCESS,		// 针对随机访问，对文件缓冲进行优化
		flagSequentialScan	= FILE_FLAG_SEQUENTIAL_SCAN,	// 针对连续访问，对文件缓冲进行优化
		flagDeleteOnClose	= FILE_FLAG_DELETE_ON_CLOSE,	// 关闭上一次打开句柄后，将文件删除，适用于临时文件
	};

	// 文件属性，可combine
	enum FileAttr
	{
		// 以下属性为 SetStatus 可以更改的
		attrNormal		= FILE_ATTRIBUTE_NORMAL,		// 一般属性
		attrHidden		= FILE_ATTRIBUTE_HIDDEN,		// 隐藏文件
		attrReadonly	= FILE_ATTRIBUTE_READONLY,		// 只读文件
		attrSystem		= FILE_ATTRIBUTE_SYSTEM,		// 系统文件
		attrTemporary	= FILE_ATTRIBUTE_TEMPORARY,		// 临时文件属性
		attrArchive		= FILE_ATTRIBUTE_ARCHIVE,		// 归档属性
		attrOffline		= FILE_ATTRIBUTE_OFFLINE,
		attrNotContentIndexed = FILE_ATTRIBUTE_NOT_CONTENT_INDEXED,

		// 以下属性为 SetStatus 不能更改的
		attrDirectory	= FILE_ATTRIBUTE_DIRECTORY,		// 目录
		attrVolume		= 0x08,							// 硬盘分区
		attrDevice		= FILE_ATTRIBUTE_DEVICE,		// 设备文件
		attrCompressed	= FILE_ATTRIBUTE_COMPRESSED,	// 已压缩
		attrEncrypted	= FILE_ATTRIBUTE_ENCRYPTED,
		attrSparseFile	= FILE_ATTRIBUTE_SPARSE_FILE,
		attrReparsePoint = FILE_ATTRIBUTE_REPARSE_POINT,
	};

	// 文件内指针偏移方式
	enum SeekPosition
	{
		seekBegin	= FILE_BEGIN,
		seekCurrent	= FILE_CURRENT,
		seekEnd		= FILE_END,
	};

	// 各种文件名称
	struct FileNames
	{
		TCHAR szFullName[MAX_NAMEBUF];	// 完整名 "C:/ABC/test.txt"
		LPCTSTR pFileName;				// 文件名 "test.txt"
		LPCTSTR pExtName;				// 扩展名 "txt"
	};

	//// 静态
	// 得到属性
	static BOOL GetAttributes(LPCTSTR pFileName, S_OUT DWORD& file_attr);

	// 设置属性
	static BOOL SetAttributes(LPCTSTR pFileName, DWORD file_attr);

	// 清空内容，清空后不占用磁盘空间
	static BOOL Clean(LPCTSTR pFileName);

	// 是否存在
	static BOOL IsExist(LPCTSTR pFileName);

	// 得到各种名称
	static BOOL GetNames(LPCTSTR pFileName, S_OUT FileNames& names);

	// 删除文件
	static BOOL Delete(LPCTSTR pFileName);

	// 重命名文件或移动文件(文件夹也可以包含子目录)
	static BOOL Move(LPCTSTR pOldFile, LPCTSTR pNewFile);

	// 复制文件
	static BOOL Copy(LPCTSTR pSrcFile, LPCTSTR pNewFile, BOOL bFailExist = TRUE);

	//// 构造
	SFile();
	virtual ~SFile();

	//// 操作
	// 打开
	BOOL Open(LPCTSTR pFileName, DWORD access_mode, DWORD share_mode = shareNone,
		DWORD file_flag = flagNormal, BOOL bInherit = FALSE);

	// 创建
	BOOL Create(LPCTSTR pFileName, DWORD access_mode, DWORD share_mode = shareNone,
		DWORD file_flag = flagNormal, BOOL bInherit = FALSE,
		const SECURITY_DESCRIPTOR* pSecurityDescriptor = NULL);

	// 关闭
	void Close();

	// 读取
	BOOL Read(S_OUT void* pData, DWORD datalenBytes, S_OUT DWORD* pReadLen = NULL);

	// 写入
	BOOL Write(const void* pData, DWORD datalenBytes, S_OUT DWORD* pWriteLen = NULL);

	// 移动文件内部指针
	BOOL Seek(SeekPosition from, INT64 offset = 0, S_OUT UINT64* pNewPosition = NULL);

	// 设置文件大小，不会缩小文件占用的物理空间
	BOOL SetSize(UINT64 size);
	
	// 刷新磁盘缓冲
	BOOL Flush();

	// 脱离已绑定文件
	HANDLE Detach();

	// 设置文件时间
	BOOL SetTime(S_OPT const STime* pCreateTime,
				 S_OPT const STime* pModifyTime,
				 S_OPT const STime* pAccessTime);

	//// 属性
	// 是否有效
	BOOL IsValid()const;
		
	// 得到文件句柄, 无效句柄为 NULL
	HANDLE GetHandle()const;

	// 得到文件名，请把此名字拷贝到自己的缓冲区
	LPCTSTR GetFullName()const;	// 完整名 "C:/ABC/test.txt"
	LPCTSTR GetName()const;		// 文件名 "test.txt"
	LPCTSTR GetExtName()const;	// 扩展名 "txt"
	
	// 得到大小
	BOOL GetSize(S_OUT UINT64& size)const;

	// 得到时间
	BOOL GetTime(S_OPT S_OUT STime* pCreateTime,
				 S_OPT S_OUT STime* pModifyTime,
				 S_OPT S_OUT STime* pAccessTime)const;
};

S_END_SL()