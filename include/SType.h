/************************************************************************\
* 文件：SType.h
*
* 说明：动态类型类
*
* 作者：Seven
\************************************************************************/ 

#pragma once
#include "CommonDef.h"
S_BEGIN_SL()

// 前置声明
class SObject;
typedef SObject* (*CreateObjectFunc)();

class S_EXPORT SType
{
	S_DISABLE_COPY(SType)

public:
	//// 操作
	// 动态创建
	SObject* CreateObject();
	
	//// 属性
	// 得到类型信息
	LPCTSTR	GetName()const;
	UINT	GetSize()const;

	//// 构造
	//类型信息的初始化，永远不要手动创建类型实例
	SType(LPCTSTR lpszName, UINT uSize, CreateObjectFunc fnCreate);
	virtual ~SType();

private:
	class S_PIMPL(SType);
	static S_PIMPL(SType)* CreatePIMPL(const SType* pPannel);
	S_PIMPL(SType)* const m_PIMPL;
};

S_END_SL()