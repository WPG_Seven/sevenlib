/************************************************************************\
* 文件：STimeSpan.h
*
* 说明：时间段类，表示某两个时间点之间的时间段
*
* 作者：Seven
\************************************************************************/ 

#pragma once
#include "CommonDef.h"
S_BEGIN_SL()

class S_EXPORT STimeSpan
{
	S_DECLARE_PIMPL(STimeSpan)

public:
	//// 构造
	STimeSpan(INT64 Secs = 0);			// 间隔为Secs秒
	STimeSpan(const STimeSpan& span);
	virtual ~STimeSpan();
	
	//// 操作
	// 设置间隔为Days天，Hours小时，Mins分，Secs秒
	void SetSpan(INT64 Secs, INT64 Mins = 0, INT64 Hours = 0, INT64 Days = 0);

	//// 属性
	// 总间隔
	INT64 GetTotalDays()const;
	INT64 GetTotalHours()const;
	INT64 GetTotalMinutes()const;
	INT64 GetTotalSeconds()const;

	// 单位进位之后的间隔
	int GetHours()const;
	int GetMinutes()const;
	int GetSeconds()const;
	
	//// 重载运算符
	STimeSpan operator+(const STimeSpan& span)const;
	STimeSpan operator-(const STimeSpan& span)const;
	STimeSpan& operator+=(const STimeSpan& span);
	STimeSpan& operator-=(const STimeSpan& span);
	STimeSpan& operator=(const STimeSpan& span);
	bool operator==(const STimeSpan& span)const;
	bool operator!=(const STimeSpan& span)const;
	bool operator<(const STimeSpan& span)const;
	bool operator>(const STimeSpan& span)const;
	bool operator<=(const STimeSpan& span)const;
	bool operator>=(const STimeSpan& span)const;
};

S_END_SL()