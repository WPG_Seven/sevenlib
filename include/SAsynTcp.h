/************************************************************************\
* 文件：SAsynTcp.h
*
* 说明：异步TCP类
*
* 作者：Seven
\************************************************************************/

#pragma once

#include "CommonDef.h"
S_BEGIN_SL()

class SIpEndpoint;
class SSocket;
class S_EXPORT SAsynTcp
{
	S_DECLARE_PIMPL(SAsynTcp);
	S_DISABLE_COPY(SAsynTcp);

public:
	static const UINT BUFFER_MAXSIZE = 4096;

	SAsynTcp();
	virtual ~SAsynTcp();

	// 得到Socket句柄
	SSocket* GetSocket()const;
	
	// 创建
	BOOL Create(const SIpEndpoint& localep);

	// 关闭
	void Close();

	// 发送信息
	BOOL SendTo(const BYTE* buf, UINT buflen,
				const SIpEndpoint& remoteep,
				S_OUT UINT* sendlen = NULL);

	// 读取缓冲区中的信息，在收到数据时，缓冲区会被后台线程调用SAsynUdp::OnRecvFrom()填充
	BOOL RecvFrom(S_OUT BYTE* buf, UINT buflen,
				  S_OUT SIpEndpoint* fromep = NULL,
				  S_OUT UINT* recvlen = NULL);

protected:
	// 收到数据时会在后台线程调用此函数，默认实现会填充缓冲区，无需自己调用
	virtual void OnRecvFrom();
};

S_END_SL()